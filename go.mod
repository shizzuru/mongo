module gitlab.com/shizzuru/mongo

go 1.13

require (
	github.com/spf13/viper v1.7.0
	go.mongodb.org/mongo-driver v1.3.4
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
)
