package hardware

// Definition is the description of a piece of hardware
type Definition struct {
	Name     string
	Model    string
	Location string
	Metadata map[string]interface{}
}
