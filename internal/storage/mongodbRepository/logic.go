package mongodbrepository

import (
	"context"
	"log"
	"time"

	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (m *mongoRepo) Store(i interface{}) (*mongo.InsertOneResult, error) {
	collection := m.Client.Database(m.dbname).Collection(viper.GetString("database_collection"))
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	return collection.InsertOne(ctx, i)
}

// Get is a generator
func (m *mongoRepo) Find(filter map[string]interface{}) <-chan map[string]interface{} {
	cGenerator := make(chan map[string]interface{})
	go func() {
		defer close(cGenerator)

		collection := m.Client.Database(m.dbname).Collection(viper.GetString("database_collection"))
		ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
		cur, err := collection.Find(ctx, filter)
		if err != nil {
			log.Fatal(err)
		}
		defer cur.Close(ctx)

		for cur.Next(ctx) {
			var result bson.M
			err := cur.Decode(&result)
			if err != nil {
				log.Fatal(err)
			}
			cGenerator <- result
		}
		if err := cur.Err(); err != nil {
			log.Fatal(err)
		}
	}()
	return cGenerator
}

// FindOneFromID .
func (m *mongoRepo) FindOneFromID(objectID primitive.ObjectID) interface{} {
	var result interface{}
	collection := m.Client.Database(m.dbname).Collection(viper.GetString("database_collection"))
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)

	filter := bson.M{"_id": objectID}
	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	err := collection.FindOne(ctx, filter).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}

	return result
}
