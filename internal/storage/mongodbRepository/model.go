package mongodbrepository

import (
	"context"
	"fmt"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/shizzuru/mongo/internal/storage"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// NewMongoRepository initialise a new database and returns a Repository
func NewMongoRepository() storage.Repository {
	host := viper.GetString("database_ip")
	port := viper.GetInt("database_port")
	dbname := viper.GetString("database_name")
	connectionString := fmt.Sprintf("mongodb://%v:%v", host, port)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connectionString))
	if err != nil {
		panic(err)
	}
	return &mongoRepo{
		Client: client,
		dbname: dbname,
	}
}

type mongoRepo struct {
	*mongo.Client
	dbname string
}
