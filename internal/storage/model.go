package storage

import (
	"go.mongodb.org/mongo-driver/mongo"
)

// Repository is the interface to access the long term storage support
type Repository interface {
	Store(interface{}) (*mongo.InsertOneResult, error)
	Find(filter map[string]interface{}) <-chan map[string]interface{}
	// FindOneFromID(primitive.ObjectID) interface{}
}
