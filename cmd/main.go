package main

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/shizzuru/mongo/internal/storage/mongodbrepository"
)

func init() {
	viper.SetConfigName("default")
	viper.SetConfigType("json")
	viper.AddConfigPath("./configs")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func errH(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	r := mongodbrepository.NewMongoRepository()

	_, err := r.Store(struct {
		Shit string `bson: "shit,omitempty"`
	}{
		Shit: "indeed",
	})
	if err != nil {
		panic(err)
	}

	for cv := range r.Find(map[string]interface{}{}) {
		for k, v := range cv {
			fmt.Printf("%v - %v\n", k, v)
		}
	}
}
